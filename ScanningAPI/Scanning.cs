﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTwain;
using NTwain.Data;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;

namespace ScanningAPI
{
    public class Scanner
    {
        public TWIdentity AppId { get; private set; }
        public TwainSession Session { get; private set; }
        public bool IsSessionOpen { get; private set; }

        public DataSource Source { get; private set; }

        public bool IsFinished
        {
            get
            {
                return isFinished;
            }

            set
            {
                isFinished = value;
            }
        }

        private bool isFinished;

        public string FileLocation { get; private set; }


        public Scanner()
        {
            Initialize();
        }

        private void Initialize()
        {
            AppId = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetExecutingAssembly());
            Session = new TwainSession(AppId);
            Session.TransferReady += Session_TransferReady;
            Session.DataTransferred += Session_DataTransferred;
            Session.TransferError += Session_TransferError;
            Session.SourceDisabled += Session_SourceDisabled;
        }

        private void Session_SourceDisabled(object sender, EventArgs e)
        {
            Source.Close();
        }

        private void Session_TransferError(object sender, TransferErrorEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Session_DataTransferred(object sender, DataTransferredEventArgs e)
        {
            using (var stream = e.GetNativeImageStream())
            {
                
                List<byte> array = new List<byte>();
                byte[] buffer = new byte[1024];
                int count = 0;
                while ((count = stream.Read(buffer, 0, 1024)) > 0)
                {
                    array.AddRange(buffer.Take(count));
                }

                Image img = (Bitmap)((new ImageConverter()).ConvertFrom(array.ToArray()));
                string fileName = @"c:\temp\test_" + DateTime.Today.Ticks + ".jpg";
                img.Save(fileName, ImageFormat.Jpeg);
                FileLocation = fileName;
                IsFinished = true;

            }
        }

        private void Session_TransferReady(object sender, TransferReadyEventArgs e)
        {
            var info = e.PendingImageInfo;
        }


        public bool OpenSession()
        {
            IsSessionOpen = Session.Open() == ReturnCode.Success;
            return IsSessionOpen;
        }

        public void Start()
        {
            Source = Session.FirstOrDefault();
            Source.Open();
            var returnCode = Source.Enable(SourceEnableMode.NoUI, false, IntPtr.Zero);
        }


        public void Close()
        {
            //if(Source.IsOpen )
            //{
            //    Source.Close();
            //}

            //if(IsSessionOpen)
            //{
            //    Session.Close();
            //}
        }




    }
}
