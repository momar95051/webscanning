﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ScanWeb.Startup))]
namespace ScanWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
