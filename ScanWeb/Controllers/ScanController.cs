﻿using System;
using System.Drawing;
using System.IO;
using System.Web.Mvc;

namespace ScanWeb.Controllers
{
    public class ScanController : Controller
    {
        // GET: Scan

        public ActionResult index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PostFiles(FormCollection data)
        {

            foreach (var key in Request.Params.Keys)
            {
                if(!key.ToString().Contains("hidden"))
                {
                    continue;
                }


                var request = Request.Params[key.ToString()];
                var encode = Server.UrlDecode(request);
                byte[] bytes = Convert.FromBase64String(encode);
                Image img = null;
                using (MemoryStream s = new MemoryStream(bytes))
                {
                    img = Image.FromStream(s);
                    img.Save(Server.MapPath("~/img/test" + key.ToString() +  ".jpeg"));
                }

            }

            return Json("success");
        }


    }
}
