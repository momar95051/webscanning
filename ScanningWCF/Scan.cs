﻿using ScanningAPI;
using System;
using System.IO;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace ScanningWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ScanService : IScanningService
    {
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string ScanFile()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            Scanner scanner = new Scanner();
            scanner.OpenSession();
            scanner.Start();
            while (!scanner.IsFinished) ;
            scanner.Close();
           return  Convert.ToBase64String(File.ReadAllBytes(scanner.FileLocation));
        }
    }
}
