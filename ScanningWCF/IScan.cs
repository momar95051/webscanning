﻿using System.ServiceModel;

namespace ScanningWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
   
    public interface IScanningService
    {
        [OperationContract]
        string ScanFile();


        // TODO: Add your service operations here
    }
}
