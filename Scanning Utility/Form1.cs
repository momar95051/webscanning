﻿using Scanning_Utility.ServiceReference1;
using ScanningAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scanning_Utility
{
    public partial class ScannerUtility : Form
    {
        public ScannerUtility()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Scanner scanner = new Scanner();
            scanner.OpenSession();
            scanner.Start();
            while (!scanner.IsFinished) ;
            scanner.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ScanningServiceClient client = new ScanningServiceClient();
            byte[] bytes = client.ScanFile();
            client.Close();
            Image img = null;
            pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            pictureBox1.Width = this.Width / 2;
            pictureBox1.Height = this.Height / 2;
            pictureBox1.Left = (this.Width - pictureBox1.Width) / 2;
            pictureBox1.Top = (this.Height - pictureBox1.Height) / 2;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                img = Image.FromStream(stream);
            }

            pictureBox1.Image = img;
            pictureBox1.Refresh();
            pictureBox1.Invalidate();
        }
    }
}
