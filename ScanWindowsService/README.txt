﻿To install the project as a service use the following command:
<Assembly Name> [ScanWindowsService.exe] install  -username:[AdministratorUserName] -password:[AdministratorPassword] -servicename:[The name to be shown on the service console] [-autostart]



- The service allows the scanning to be done via a windows service that runs as an administrator account that can access the actual port that listens for scanning request
- It hosts the IScanService Windows Communication Foundation based service which in turns talks to the first available scanner via a NTWAIN layer adapter
-   

